/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package humanresources;

/**
 *
 * @author ngocb
 */
public class Manager extends Staff implements ICalculator {

    private int position;
    float salary;

    public Manager(String code, String name, int age, float salary_ratio,
            String joinDate, String departmentName, int daysOff, int position) {

        super.setCode(code);
        super.setName(name);
        super.setAge(age);
        super.setSalaryRatio(salary_ratio);
        super.setJoinDate(joinDate);
        super.setDepartmentName(departmentName);
        super.setDaysOff(daysOff);

        this.position = position;
    }

    public Manager() {
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public float calculateSalary() {

        salary = super.getSalaryRatio() * 5000000 + position;
        return salary;

    }

    //@Override
    // public int compareTo(Staff t) {
    // }
    @Override
    public void displayInformation() {

        String p = null;

        if (position == 8000000) {
            p = "Business Leader";
        } else if (position == 5000000) {
            p = "Project Leader";
        } else if (position == 6000000) {
            p = "Technical Leader";
        }

        String row = String.format("%6s%20s%5d%15.2f%12s%12s%10d%12s%20s",
                super.getCode(), super.getName(), super.getAge(),
                super.getSalaryRatio(), super.getJoinDate(),
                super.getDepartmentName(), super.getDaysOff(), "Manager", p);
        System.out.print(row);

    }

    @Override
    public int compareTo(Staff t) {
        salary = ((Manager) t).calculateSalary();
        return 0;

    }

}
