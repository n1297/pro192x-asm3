/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package humanresources;

/**
 *
 * @author ngocb
 */
public class Department {
    
    private String departmentId;
    private String departmentName;
    private int departmentStaffQuantity;

    public Department(String departmentId, String departmentName,
            int departmentStaffQuantity) {
        
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.departmentStaffQuantity = departmentStaffQuantity;
    }

    public Department() {
        
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public int getDepartmentStaffQuantity() {
        return departmentStaffQuantity;
    }

    public void setDepartmentStaffQuantity(int departmentStaffQuantity) {
        this.departmentStaffQuantity = departmentStaffQuantity;
    }

    @Override
    public String toString() {
        String row = String.format("%6s%20s%15d", this.departmentId,
                this.departmentName, this.departmentStaffQuantity);
        System.out.println(row);
        return row; //To change body of generated methods, choose Tools | Templates.
    }

   
}
