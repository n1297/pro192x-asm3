/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package humanresources;

import java.util.Objects;

//Temporarily use inheritance class Department

public abstract class Staff extends Department implements Comparable<Staff>{
   
    private String code;
    private String name;
    private int age;
    private float salaryRatio;
    private String joinDate;
    private String departmentName;
    int daysOff;


    public Staff(String code, String name, int age, float salaryRratio,
            String joinDate, String departmentName, int daysOff) {
        
        this.code = code;
        this.name = name;
        this.age = age;
        this.salaryRatio = salaryRratio;
        this.joinDate = joinDate;
        super.setDepartmentName(departmentName);
        this.daysOff = daysOff;
    }

    public Staff() {
        
    }

    
    public String getCode() {
        return code;
    }


    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public float getSalaryRatio() {
        return salaryRatio;
    }

    public void setSalaryRatio(float salaryRatio) {
        this.salaryRatio = salaryRatio;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public int getDaysOff() {
        return daysOff;
    }

    public void setDaysOff(int daysOff) {
        this.daysOff = daysOff;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Staff other = (Staff) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        return true;
    }
    
    public abstract void displayInformation();

    

}
