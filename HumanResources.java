/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package humanresources;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import humanresources.Staff;
import humanresources.Manager;
import static java.util.Collections.max;
import static java.util.Collections.min;

/**
 *
 * @author ngocb
 */
public class HumanResources {

    static Scanner sc = new Scanner(System.in);
    static List<Staff> staff = new ArrayList<>();
    static List<Department> department = new ArrayList<>();
  
    /*
    First row of staff information
    */
    public static void firstRow() {

        System.out.printf("%6s", "ID");
        System.out.printf("%20s", "Name");
        System.out.printf("%5s", "Age");
        System.out.printf("%15s", "Salary Ratio");
        System.out.printf("%12s", "Date Join");
        System.out.printf("%12s", "Department");
        System.out.printf("%10s", "Day Off");
        System.out.printf("%12s", "Staff Type");
        System.out.printf("%20s", "Position");
        System.out.printf("%12s", "Over Time");
    }

    public static void displayStaffInformation() {

        firstRow();
        System.out.println("");

        for (Staff i : staff) {
            i.displayInformation();
            System.out.println("");

        }
    }

    public static void displayDepartmentInformation() {

        System.out.printf("%6s", "ID");
        System.out.printf("%20s", "Deparmentname");
        System.out.printf("%15s\n", "Staff Quantity");

        for (Department i : department) {
            i.toString();
        }
    }

    public static void displayStaffEachDepartment() {

        firstRow();
        System.out.println("");

        for (Department i : department) {
            for (Staff j : staff) {
                if (i.getDepartmentName() == j.getDepartmentName()) {
                    j.displayInformation();
                    System.out.println("");
                }
            }
        }
    }

    /*A subclass for addStaff().
    It imports the common part of Manager and Employee    
     */
    public static void enterStaff(Staff m) {
        
        sc = new Scanner(System.in);
        System.out.println("Please enter Staff ID :");
        String code = sc.nextLine();
        m.setCode(code);

        sc = new Scanner(System.in);
        System.out.println("Please enter Staff Name :");
        String name = sc.nextLine();
        m.setName(name);

        System.out.println("Please enter Staff Age :");
        int age = sc.nextInt();
        m.setAge(age);

        System.out.println("Please enter Staff Salary Ratio :");
        float salaryRatio = sc.nextFloat();
        m.setSalaryRatio(salaryRatio);

        System.out.println("Please enter Staff day of join work:");
        sc = new Scanner(System.in);
        String joinDate = sc.nextLine();
        m.setJoinDate(joinDate);

        System.out.println("Please enter Staff Department");
        String department = sc.nextLine();
        m.setDepartmentName(department);

        System.out.println("Please enter Staff number of day off");
        int dayOff = sc.nextInt();
        m.setDaysOff(dayOff);
    }

    //Select position of Manager and add responsibility salary to Manager 
    public static void selectPossition(Manager manager) {

        System.out.println("Please press the corresponding number"
                + " to select position of Manager:");
        System.out.println("1.Business Leader");
        System.out.println("2.Project Leader");
        System.out.println("3.Technical Leader");

        int po = sc.nextInt();
        int position;
        switch (po) {
            case 1:
                position = 8000000;
                manager.setPosition(position);
                break;
            case 2:
                position = 5000000;
                manager.setPosition(position);
                break;
            case 3:
                position = 6000000;
                manager.setPosition(position);
                break;
            default:
                System.err.println("Wrong format, please re-enter");
                selectPossition(manager);
        }

    }

    // add a Staff with object(Manager orr Employee) to List
    public static void addStaff() {

        //Press to select type of Staff. Employee or Manager?
        System.out.println("Press the corresponding number to select type"
                + " staff");
        System.out.println("1.Employee");
        System.out.println("2.Manager");

        int selection = sc.nextInt();
        switch (selection) {
            case 1:

                Employee em = new Employee();
                enterStaff(em);
                System.out.println("Please enter over time");
                int overtime = sc.nextInt();
                em.setOverTime(overtime);
                staff.add(em);
                break;

            case 2:

                Manager manager = new Manager();
                enterStaff(manager);
                selectPossition(manager);
                staff.add(manager);
                break;

            default:
                System.err.println("Wrong format, please re-enter");
                addStaff();
        }
    }

    //Search staff. Keyword is Name or id ?
    public static void searchStaff() {

        System.out.println("Please enter keyworld");
        sc = new Scanner(System.in);
        String keyworld = sc.nextLine();
        int notthing = 0;
        for (Staff i : staff) {
            if (i.getCode().equals(keyworld) || i.getName().equals(keyworld)) {
                firstRow();
                i.displayInformation();
                notthing += 1;
            }

        }
        if (notthing == 0) {
            System.out.println("This information is not available\n");
        }
    }

    public static void displayStaffSlary(float salary) {
        System.out.printf(" %12.2f\n ", salary);
    }

    public static void listStaffSalary() {

        float salary = 0;
        for (Staff i : staff) {
            i.displayInformation();

            displayStaffSlary(salary);
        }
    }

    public static float caculatorSalary(Staff staff1) {
        float salary = 0;
        if (staff1 instanceof Manager) {
            salary = ((Manager) staff1).calculateSalary();

        } else if (staff1 instanceof Employee) {
            salary = ((Employee) staff1).calculateSalary();
        }
        return salary;
    }

    public static void listStaffSlaryAscending() {
        float salary = 0;
        List<Staff> temp = new ArrayList<>();
        List<Staff> staffSalaryAscending = new ArrayList<>();
        List<Float> listsalary = new ArrayList<>();
        float max = 0;
        for (Staff staff1 : staff) {

            salary = caculatorSalary(staff1);
            listsalary.add(salary);
        }
        max = min(listsalary);
        for (Staff staff1 : staff) {

            if (caculatorSalary(staff1) > max) {
                staffSalaryAscending.add(staff1);
                max = caculatorSalary(staff1);
            } else {
                int i = 0;
                for (Staff s : staff) {

                    if (caculatorSalary(s) < max) {
                        i++;
                    }
                }
                staffSalaryAscending.add(staffSalaryAscending.size() - i, staff1);

            }

        }

        for (Staff staff1 : staffSalaryAscending) {
            staff1.displayInformation();
            displayStaffSlary(caculatorSalary(staff1));
            System.out.println("");
        }
    }

    private static void ListStaffSlaryDescending() {
        float salary = 0;
        List<Staff> temp = new ArrayList<>();
        List<Staff> staffSalaryDescending = new ArrayList<>();
        List<Float> listsalary = new ArrayList<>();
        float min = 0;
        for (Staff staff1 : staff) {

            salary = caculatorSalary(staff1);
            listsalary.add(salary);
        }
        min = max(listsalary);
        for (Staff staff1 : staff) {

            if (caculatorSalary(staff1) < min) {
                staffSalaryDescending.add(staff1);
                min = caculatorSalary(staff1);
            } else {
                int i = 0;
                for (Staff s : staff) {

                    if (caculatorSalary(s) > min) {
                        i++;
                    }
                }
                staffSalaryDescending.add(staffSalaryDescending.size() - i, staff1);

            }

        }
        firstRow();
        System.err.println("Salary");
        for (Staff staff1 : staffSalaryDescending) {
            staff1.displayInformation();
            displayStaffSlary(caculatorSalary(staff1));
            System.out.println("");
        }
    }

    //After running the program, do you continue?
    public static void continueProgram() {

        System.out.println(" Do you want to continue ?");
        System.out.println(" 1.Press 1 to continue");
        System.out.println(" 2.Press another to exit");

        int selection = sc.nextInt();
        if (selection == 1) {
            selectFunction();
        } else {
            System.out.println("The program was finished");
            System.exit(0);
        }
    }

    //Select a function
    public static void selectFunction() {

        System.out.println("\nPlease press the corresponding number"
                + " to select the function\n");

        System.out.println("1.Company staff information");
        System.out.println("2.Company department infomation");
        System.out.println("3.List staff in each department");
        System.out.println("4.Add new staff");
        System.out.println("5.Search staff");
        System.out.println("6.List staff salary");
        System.out.println("7.List staff salary in ascending order");
        System.out.println("8.List staff salary in descending order");
        System.out.println("9.Exit");

        int selection = sc.nextInt();

        switch (selection) {
            case 1:
                System.out.println("1.Company staff information");
                displayStaffInformation();
                break;

            case 2:
                System.out.println("2.Company department infomation");
                displayDepartmentInformation();
                break;

            case 3:
                System.out.println("3.List staff in each department");
                displayStaffEachDepartment();
                break;

            case 4:
                System.out.println("4.Add new staff");
                addStaff();
                break;

            case 5:
                System.out.println("5.Search staff");
                searchStaff();
                break;

            case 6:
                System.out.println("6.List staff salary");
                listStaffSalary();
                break;
            case 7:
                System.out.println("7.List staff salary in ascending order");
                listStaffSlaryAscending();
                break;

            case 8:
                System.out.println("8.List staff salary in descending order");
                ListStaffSlaryDescending();
                break;
            case 0:
                System.out.println("The program was finished");
                System.exit(0);

            default:
                System.err.println("Wrong format, please re-enter");
                selectFunction();
        }
        continueProgram();
    }

    public static void main(String[] args) {

        exampleElementOfList();
        selectFunction();
    }

    public static void exampleElementOfList() {
        //Two element array with Manager object
        Manager manager1 = new Manager("001", "Pham Thi Thuy Anh", 27,
                (float) 1.6, "1/9/2016", "Personnel", 12, 5000000);
        Manager manager2 = new Manager("003", "Dang Tuan Tai", 27,
                (float) 1.8, "1/9/2016", "Accounting", 2, 8000000);
        staff.add(manager1);
        staff.add(manager2);

        //Third element array with Employee object
        Employee employee = new Employee("002", "Pham Van Bach", 23,
                (float) 1.3, "1/9/2020", "Technical", 3, 27);
        staff.add(employee);

        //Three elements of Department
        Department department1 = new Department("ACC", "Accounting", 9);
        Department department2 = new Department("PER", "Personnel", 11);
        Department department3 = new Department("TEC", "Technical", 13);
        department.add(department1);
        department.add(department2);
        department.add(department3);
    }

}
