/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package humanresources;
import humanresources.Staff;
/**
 *
 * @author ngocb
 */
public class Employee extends Staff implements ICalculator{

    Staff staff;
    float salary;
    int overTime;

    public Employee(String code, String name, int age, float salaryRatio,
            String joinDate, String department, int daysOff,int overTime) {
        
        super.setCode(code);
        super.setName(name);
        super.setAge(age);
        super.setSalaryRatio(salaryRatio);
        super.setJoinDate(joinDate);
        super.setDepartmentName(department);
        super.setDaysOff(daysOff);
        
        this.overTime = overTime;
    }
    
    public Employee(){}

    public int getOverTime() {
        return overTime;
    }

    public void setOverTime(int overTime) {
        this.overTime = overTime;
    }

    
    @Override
    public void displayInformation() {
        
        String row= String.format("%6s%20s%5d%15.2f%12s%12s%10d%12s%20s%12d",
                super.getCode(),super.getName(),super.getAge(),
                super.getSalaryRatio(),super.getJoinDate(),
                super.getDepartmentName(),super.getDaysOff(),"Employee","",overTime);
        System.out.print(row);
    }

    
    @Override
    public float calculateSalary() {
        salary = (super.getSalaryRatio()*3000000)+(overTime*200000);
        return salary;
    }

    @Override
    public int compareTo(Staff t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
